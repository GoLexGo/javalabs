package com.javaLabs.factorial;
/*Ввести с клавиатуры строку, представ-ляющую некоторое слова.
Это слово должно состоять только из заглавных или строчных латинских букв.
Проверьте, верно ли что это слово читается одинаково как справа налево,
так и слева направо (то есть является палин-дромом),
 если считать заглавные и строчные буквы не различающимися.
 Выведите слово YES, если слово является палиндромом и слово NO, если не является.
  При решении этой задачи нельзя пользоваться вспомога-тельными массивами или строками.*/

import java.util.Scanner;

public class Lab2
{

    public static void main(String[] args)
    {
        StringChecker stringChecker = new StringChecker();
    }

    public static class StringChecker implements IPrintable
    {
        char[] inputData;

        public StringChecker()
        {
            CheckInput();
        }

        private void CheckInput()
        {
            Scanner scanner = new Scanner(System.in);
            do
            {
                Print("Input line:");
                inputData = scanner.nextLine().toCharArray();
                if(CheckLowerCase())
                {
                    Print("Detect LowerCases!");
                    break;
                }
                else if(CheckUpperCase())
                {
                    Print("Detect UpperCases!");
                    break;
                }
                Print("Detect no upper- or lowercases!");


            }while (true);

            if(CheckPalindrom())
            {
                Print(AnswerContainer.correctAnswer);
            }
            else
            {
                Print(AnswerContainer.wrongAnswer);
            }

        }

        private boolean CheckLowerCase()
        {
            int lowerCaseCount = 0;

            for(int i = 0;i< inputData.length;i++)
            {
                if(Character.isLowerCase(inputData[i]) || Character.isSpaceChar(inputData[i]))
                {
                    lowerCaseCount++;
                }
            }

            return lowerCaseCount == inputData.length? true:false;
        }

        private boolean CheckUpperCase()
        {
            int upperCaseCount = 0;

            for(int i = 0;i< inputData.length;i++)
            {
                if(Character.isUpperCase(inputData[i]) || Character.isSpaceChar(inputData[i]))
                {
                    upperCaseCount++;
                }
            }

            return upperCaseCount == inputData.length? true:false;
        }

        private boolean CheckPalindrom()
        {
            int j = 0;
            for(int i = inputData.length - 1; i >= 0; i--)
            {
                //Print(inputData[i]+" <-> "+inputData[j]);
                if( inputData[i] != inputData[j])
                {
                    return false;
                }

                j++;

            }
            return true;
        }

    }
    public static class AnswerContainer
    {
        public static final String correctAnswer = "Yes";
        public static final String wrongAnswer = "No";
    }
}
