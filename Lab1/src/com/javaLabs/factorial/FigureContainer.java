package com.javaLabs.factorial;

import java.util.ArrayList;

public class FigureContainer implements IPrintable
{
    Figure[] figures;

    public FigureContainer(Figure[] circles)
    {
        this.figures = circles;
    }

    public FigureContainer(Figure figures)
    {
        this.figures = new Figure[]{figures};
    }

    public void FindEqualSquaers()
    {
        ArrayList<Figure> found = new ArrayList<Figure>();
        for(int i = 1;i<figures.length;i++)
        {
            for(int j = 0;j<figures.length;j++)
            {
                if(i == j) continue;

                if(figures[i].GetSquare() == figures[j].GetSquare())
                {
                    if(found.contains(figures[i])) continue;
                    found.add(figures[i]);

                    Print(figures[i].GetSignature() + " № "+(i+1)+" and №"+(j+1));
                }
            }

        }
        if(found.isEmpty())
        {
            Print("No matches");
        }
    }

    public void ToString()
    {
        for(int i = 0;i<figures.length;i++)
        {
            Print(figures[i].toString());
        }
    }
}
