package com.javaLabs.factorial;

import java.util.Scanner;

public class Lab5
{

    public static void main(String[] args)
    {
        Matrix matrix = new Matrix();
        matrix.Output();
        matrix.CheckIfMagicSquare();
    }

}

class Matrix implements IPrintable
{
    public int[][] matrix;
    Scanner scanner;
    int n;//raw
    int m;//column

    public Matrix()
    {
        scanner = new Scanner(System.in);

        InitDimension();

        matrix = new int[n][m];

        for (int i = 0; i < matrix.length; i++)
        {
            for (int j = 0; j < matrix[i].length; j++)
            {
                Print("[" + (i + 1) + "][" + (j + 1) + "]: ");
                matrix[i][j] = scanner.nextInt();
            }
        }
    }

    private void InitDimension()
    {
        Print("Raw(n) and Columns(m) must be equal!");
        do
        {
            n = InputValue("n");
            m = InputValue("m");
        } while (n != m);

    }

    private int InputValue(String message)
    {
        int result = 0;
        do
        {
            Print("\nInput " + message + ": ");
            result = scanner.nextInt();
        } while (result <= 0);

        return result;
    }

    private void EndL()
    {
        System.out.println();
    }

    private void PrintElem(int i, int j)
    {
        System.out.print("[" + (i + 1) + "][" + (j + 1) + "]: " + matrix[i][j] + "\t");
    }


    private Boolean isMagicSquare()
    {
        int side = matrix.length;
        int magicNum = 0;
        for (int x = 0; x < side; ++x)
        {
            magicNum += matrix[0][x];
        }

        int sumD = 0;
        for (int x = 0; x < side; ++x)
        {
            int sumX = 0;
            int sumY = 0;
            for (int y = 0; y < side; ++y)
            {
                sumX += matrix[x][y];
                sumY += matrix[y][x];
            }
            sumD += matrix[x][x];
            if (sumX != magicNum || sumY != magicNum)
            {
                return false;
            }
        }
        return sumD == magicNum;
    }

    public void CheckIfMagicSquare()
    {
        Print(isMagicSquare()?"It's a magic square":"It's not a magic square");
    }

    public void Output()
    {
        for (int i = 0; i < matrix.length; i++)
        {
            for (int j = 0; j < matrix[i].length; j++)
            {
                PrintElem(i, j);
            }
            EndL();
        }
    }


    @Override
    public void Print(Object value)
    {
        System.out.print(value);
    }
}

