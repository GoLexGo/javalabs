package com.javaLabs.factorial;

import java.util.Scanner;

public class Kvadr extends Figure implements IInputChecker
{
    private float a; // one side of kvadr

    public Kvadr()
    {
        this.a = 0;
        this.center = IVector2.zero;
    }

    public Kvadr(float a)
    {
// this.a = a;
    //    this.center = IVector2.zero;
        this(IVector2.zero,a);        //new Kvadr(Vector2.zero, a);
    }

    public Kvadr(Vector2 center, float a)
    {
        this.center = center;
        this.a = a;
    }

    public Kvadr(Scanner scanner)
    {
        super.InitCenter(scanner);
        a = InputCheckInt(scanner, "A: ", true);
    }

    @Override
    public String GetSignature()
    {
        return "Kvadrat";
    }

    @Override
    public double GetSquare()
    {
        return Math.pow(a, 2);
    }

    @Override
    public String toString()
    {
        return super.toString() + " A: "+a;
    }

    public boolean equels(Kvadr kvadr)
    {
        return kvadr.a == this.a;

    }
}
