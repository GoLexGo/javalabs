package com.javaLabs.factorial;

import java.util.Scanner;

public class Lab3
{
    public static void main(String[] args)
    {
        ATM terminal = new ATM(1, 9999);
        System.out.println();

    }

}
 class CurrencyContainer
{
    public   Currency UAH = new Currency("UAH", "гривна");
    public   Currency USD = new Currency("USD", "доллар");
    public   Currency EUR = new Currency("EUR", "евро");
    public   Currency RUB = new Currency("RUB", "рубль");
    public static final String Message = "Avaible currency";

    public CurrencyContainer()
    {

    }

    public  Currency GetCurrencyByName(String nameCurrency)
    {
        return UAH.IsEqual(nameCurrency) ? UAH
                : USD.IsEqual(nameCurrency) ? USD
                : EUR.IsEqual(nameCurrency) ? EUR
                : RUB.IsEqual(nameCurrency) ? RUB
                : new Currency("Unsigned","none");
    }

    public  void ShowAvaibleCurrency()
    {
        OutputUtils.Print(Message);
        UAH.Output();
        Separator();
        USD.Output();
        Separator();
        EUR.Output();
        Separator();
        RUB.Output();
        OutputUtils.EndL();

    }

    public  boolean IsEquals(String value)
    {
        return UAH.IsEqual(value) ? true
                : USD.IsEqual(value) ? true
                : EUR.IsEqual(value) ? true
                : RUB.IsEqual(value) ? true
                : false;
    }

    private  void Separator()
    {
        OutputUtils.PrintInLine(" ,");
    }

}

class Declension
{
    public  String[][] sex;// = {{"", "один", "два", "три", "четыре", "пять", "шесть", "семь", "восемь", "девять"}, {"", "одна", "две", "три", "четыре", "пять", "шесть", "семь", "восемь", "девять"},};
    public  String[] str100;// = {"", "сто", "двести", "триста", "четыреста", "пятьсот", "шестьсот", "семьсот", "восемьсот", "девятьсот"};
    public  String[] str11;// = {"", "десять", "одиннадцать", "двенадцать", "тринадцать", "четырнадцать", "пятнадцать", "шестнадцать", "семнадцать", "восемнадцать", "девятнадцать", "двадцать"};
    public  String[] str10;// = {"", "десять", "двадцать", "тридцать", "сорок", "пятьдесят", "шестьдесят", "семьдесят", "восемьдесят", "девяносто"};
    public  String[][] forms;// = {{"копейка", "копейки", "копеек", "1"}, {"рубль", "рубля", "рублей", "0"}, {"тысяча", "тысячи", "тысяч", "1"}, {"миллион", "миллиона", "миллионов", "0"}, {"миллиард", "миллиарда", "миллиардов", "0"}, {"триллион", "триллиона", "триллионов", "0"},

    // можно добавлять дальше секстиллионы и т.д.
    //};
    public Declension()
    {
    }

}
 class ATM implements IPrintable // terminal
{
    private int value;
    private String currencyName;

    public ATM(int lowerLimit, int upperLimit)
    {
        InputCorrectValue(lowerLimit, upperLimit);
        InputCurrencySignature();
    }

    private void InputCorrectValue(int lowerLimit, int upperLimit)
    {
        Scanner scanner = new Scanner(System.in);
        Print("Value must be between [ > " + lowerLimit + " | < " + upperLimit + "]");

        do
        {
            Print("Input value: ");
            value = scanner.nextInt();
        } while (value < lowerLimit || value > upperLimit);

    }

    private void InputCurrencySignature()
    {
        CurrencyContainer currencyContainer = new CurrencyContainer() ;
        currencyContainer.ShowAvaibleCurrency();
        Scanner scanner = new Scanner(System.in);

        do
        {
            Print("Input currency name: ");
            currencyName = scanner.nextLine();

        } while (!currencyContainer.IsEquals(currencyName));

        MoneyUtil moneyUtil = new MoneyUtil(value);
        Print(moneyUtil.ToString(currencyContainer.GetCurrencyByName(currencyName)));

    }


}