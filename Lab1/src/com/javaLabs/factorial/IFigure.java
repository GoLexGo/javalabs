package com.javaLabs.factorial;

public interface IFigure
{
    public static Figure GetFigure()
    {
        return new Figure()
        {
            @Override
            public double GetSquare()
            {
                return 0;
            }
        };
    }

    public static Circle GetCircle()
    {
        return new Circle();
    }

    public static Kvadr GetKvadr()
    {
        return new Kvadr();
    }
}
