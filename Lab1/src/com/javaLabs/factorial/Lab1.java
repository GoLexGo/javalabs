package com.javaLabs.factorial;

import java.util.Scanner;

import static java.lang.Math.pow;

public class Lab1
{
    public static void main(String[] args)
    {
        SpeedEqutation speedEqutation = new SpeedEqutation();
        speedEqutation.OutputS();
        speedEqutation.OutputV();
    }

    public static class SpeedEqutation implements IPrintable
    {
        private double S;
        private double derivativeS;// производной от S
        private double t;
        private double v;

        public SpeedEqutation()
        {
            Scanner scanner = new Scanner(System.in);
            Print("Input t: ");
            t = scanner.nextInt();
        }

        public void OutputS()
        {
            CalculateS();
            Print("S = " + S);
        }

        public void OutputV()
        {
            Print("V = " + CalculuteDerivativeS());
        }

        void CalculateS()
        {
            S = pow(t, 3) - pow(t, 2) * 3 + 2;
        }

        double CalculuteDerivativeS()// calculate derivative s
        {
            return 3 * pow(t, 2) - t * 6;
        }

    }



}

