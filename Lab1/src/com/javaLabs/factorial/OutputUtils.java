package com.javaLabs.factorial;

public class OutputUtils
{
    public static void Print(Object value)
    {
        System.out.println(value);
    }

    public static void PrintInLine(Object value)
    {
        System.out.print(value);
    }

    public static void EndL() // end line
    {
        System.out.println();
    }

}
