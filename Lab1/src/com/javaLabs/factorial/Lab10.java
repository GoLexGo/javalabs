package com.javaLabs.factorial;

import java.util.Scanner;

public class Lab10
{
    public static void main(String[] args)
    {
        FigureContainer figureContainer1 = new FigureContainer(new Circle(new Scanner(System.in)));
        figureContainer1.ToString();

        FigureContainer figureContainer2 = new FigureContainer(new Kvadr(new Scanner(System.in)));
        figureContainer2.ToString();

    }
}
