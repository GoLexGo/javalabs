package com.javaLabs.factorial;

public class Lab9
{
    public static void main(String[] args)
    {
        Figure figure = IFigure.GetKvadr();
        OutputUtils.Print(figure.GetSignature());
        figure = IFigure.GetCircle();
        OutputUtils.Print(figure.GetSignature());
        figure = IFigure.GetFigure();
        OutputUtils.Print(figure.GetSignature());
    }
}
