package com.javaLabs.factorial;

import java.util.Random;
import java.util.Scanner;

public class Vector2 implements IPrintable, IInputChecker
{
    protected int x;
    protected int y;

//    public static Vector2 zero = new Vector2();
    public Vector2()
    {
        x = 0;
        y = 0;
    }
    public Vector2(int x, int y)
    {
        this.x = x;
        this.y = y;
    }

    public Vector2(Scanner scanner)
    {
        x = InputCheckInt(scanner, "Center(x): ", false);
        y = InputCheckInt(scanner, "Center(y): ", false);
    }

    public String toString()
    {
        return "("+this.x+", "+this.y+"); ";
    }

    public void Modify( Vector2 right)
    {
        this.x += right.x;
        this.y += right.y;
    }

    public void ModifyRandom(int min, int max)
    {
        this.x = GetRandom(min, max);
        this.y = GetRandom(min, max);
    }

    public int GetRandom(int min, int max)
    {
        int diff = max - min;
        Random random = new Random();
        int i = random.nextInt(diff + 1);
        i += min;
        return i;
    }

    public double GetDistance(Vector2 vector2)
    {
        return Math.sqrt( Math.pow(this.x - vector2.x, 2) + Math.pow(this.y - vector2.y, 2) );
    }

}
