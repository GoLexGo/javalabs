package com.javaLabs.factorial;

import java.util.Scanner;

public class Lab4
{
    public static void main(String[] args)
    {
        FIO fio = new FIO();
        fio.OutputFIO(10);
    }

}
class FIO implements IPrintable
{
    String name;
    String surname;
    Scanner scanner;

    public FIO()
    {
        scanner = new Scanner(System.in);
        name = InputValue("Name:");
        surname = InputValue("Surname:");
    }
    private String InputValue(String message)
    {
        Print(message);
        return scanner.nextLine();
    }

    public void OutputFIO(int count)
    {
        count = count <= 0 ? 1 : count;
        for(int i = 0;i<count;i++)
        {
            Output(name,"["+(i+1)+"] Name: ");
            Output(surname,"["+(i+1)+"]Surname: ");
        }
    }

    private void Output(Object value, String message)
    {
        Print(message+": "+value);
    }

}
