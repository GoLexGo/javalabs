package com.javaLabs.factorial;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Scanner;

public class Lab6
{
    public static void main(String[] args)
    {

        OutputUtils.Print("Вывести заданное в командной строке ко-личество случайных чисел с переходом на новоую строку");
        for (String i : args)
        {
            OutputUtils.Print(i);
        }
        OutputUtils.Print("Вывести заданное в командной строке ко-личество случайных чисел без перехода на новоую строку");

        for (String i : args)
        {
            OutputUtils.PrintInLine(i+" ");
        }
        OutputUtils.EndL();
        OutputUtils.Print("Ведите ваше имя и фамилию: ");
        Scanner scanner = new Scanner(System.in);
        String name = scanner.nextLine(); // next line

        OutputUtils.Print("Hello " + name);

        OutputUtils.Print("\nВедите дату вашего рождения: ");

        Scanner sc = new Scanner(System.in); // дата введенная с клавиатуры
        OutputUtils.Print("Введите дату (yyyy-MM-dd HH:mm): ");

        String dateString = sc.nextLine();// "2016-11-04 11:30";

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");

        LocalDateTime userLocalDateTime = LocalDateTime.parse(dateString, formatter);
       if(userLocalDateTime.getMonth().getValue() ==  LocalDateTime.now().getMonth().getValue())
       {

           int userDayBirthday = userLocalDateTime.getDayOfMonth();
           int currentDay = LocalDateTime.now().getDayOfMonth();
           int deltaDay = currentDay - userDayBirthday;
           if(deltaDay < 0)
           {
               OutputUtils.Print("\nДень рождение прошло (кол-во дней): " + Math.abs(deltaDay) );
           }
           else if(deltaDay > 0)
           {
               OutputUtils.Print("\nДо дня рождения осталось (кол-во дней): " + deltaDay);
           }
           else
           {
               OutputUtils.Print("\nПоздравляем с днем рождения!");
           }
       }



    }

}
