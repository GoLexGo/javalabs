package com.javaLabs.factorial;

public interface IPrintable
{
    public default void Print(Object value)
    {
        OutputUtils.Print(value);
    }
}
