package com.javaLabs.factorial;

public class Modul1
{
    public static void main(String[] args)
    {
        final int min = -99;
        final int max = 99;

        Circle2d circle2d = new Circle2d();
        circle2d.OutputShape();

        circle2d.Move(new Vector2(11, -22));
        circle2d.Resize(2f);
        circle2d.OutputShape();

        circle2d.MoveRandom(min, max);
        circle2d.Resize(22f);

        circle2d.OutputShape();

        circle2d.Resize(-22f);
        circle2d.OutputShape();

        //Circle2d circle2dWithScanner = new Circle2d(new Scanner(System.in));
        //circle2dWithScanner.OutputShape();
        Circle2d circle2dTest = new Circle2d();

        circle2dTest.SetCenter(new Vector2(11, -22));
        circle2dTest.Resize(22f);
        circle2dTest.OutputShape();

        Circle2d circle2dTest2 = new Circle2d();

        circle2dTest2.SetCenter(new Vector2(33, -22));
        circle2dTest2.Resize(22f);
        circle2dTest2.OutputShape();

        circle2dTest2.ShowTouch(circle2dTest);

    }

}
