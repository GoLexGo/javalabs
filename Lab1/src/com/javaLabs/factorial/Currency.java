package com.javaLabs.factorial;

public class Currency
{
    public String currencyName;
    public String currencyDescription;
    public Declension declension;

    public Currency(String currencyName, String currencyDescription)
    {
        this.currencyName = currencyName;
        this.currencyDescription = currencyDescription;
        InitCurrency();
        InitForms();
    }

    public void Output()
    {
        OutputUtils.PrintInLine(currencyName + " (" + currencyDescription + ")");
    }

    public boolean IsEqual(String value)
    {
        return currencyName.equals(value);
    }

    private void InitCurrency()
    {
        declension = new Declension();
        declension.sex = new String[][]{{"", "один", "два", "три", "четыре", "пять", "шесть", "семь", "восемь", "девять"}, {"", "одна", "две", "три", "четыре", "пять", "шесть", "семь", "восемь", "девять"},};
        declension.str100 = new String[]{"", "сто", "двести", "триста", "четыреста", "пятьсот", "шестьсот", "семьсот", "восемьсот", "девятьсот"};
        declension.str11 = new String[]{"", "десять", "одиннадцать", "двенадцать", "тринадцать", "четырнадцать", "пятнадцать", "шестнадцать", "семнадцать", "восемнадцать", "девятнадцать", "двадцать"};
        declension.str10 = new String[]{"", "десять", "двадцать", "тридцать", "сорок", "пятьдесят", "шестьдесят", "семьдесят", "восемьдесят", "девяносто"};
    }

    public void ShowForms()
    {
        if(declension.forms == null)
        {
            System.out.print("NULL");
            return;
        }
        for(int i =0;i<declension.forms.length;i++)
        {
            for (int j = 0; j < declension.forms[i].length; j++)
            {
                System.out.print(declension.forms[i][j]);
            }
        }
    }

    private void InitForms()
    {
        switch (currencyDescription)
        {

            case "гривна":
            {
                declension.forms = new String[][]{{"копейка", "копейки", "копеек", "1"}, {"гривна", "гривны", "гривен", "0"}, {"тысяча", "тысячи", "тысяч", "1"}, {"миллион", "миллиона", "миллионов", "0"}, {"миллиард", "миллиарда", "миллиардов", "0"}, {"триллион", "триллиона", "триллионов", "0"},};
                break;
            }
            case "доллар":
            {
                declension.forms = new String[][]{{"цент", "цента", "центов", "1"}, {"доллар", "доллара", "долларов", "0"}, {"тысяча", "тысячи", "тысяч", "1"}, {"миллион", "миллиона", "миллионов", "0"}, {"миллиард", "миллиарда", "миллиардов", "0"}, {"триллион", "триллиона", "триллионов", "0"},};

                break;
            }

            case "рубль":
            {
                declension.forms = new String[][]{{"копейка", "копейки", "копеек", "1"}, {"рубль", "рубля", "рублей", "0"}, {"тысяча", "тысячи", "тысяч", "1"}, {"миллион", "миллиона", "миллионов", "0"}, {"миллиард", "миллиарда", "миллиардов", "0"}, {"триллион", "триллиона", "триллионов", "0"},};

                break;
            }
            case "евро":
            {
                declension.forms = new String[][]{{"евроцент", "евроцента", "евроцентов", "1"}, {"евро", "евро", "евро", "0"}, {"тысяча", "тысячи", "тысяч", "1"}, {"миллион", "миллиона", "миллионов", "0"}, {"миллиард", "миллиарда", "миллиардов", "0"}, {"триллион", "триллиона", "триллионов", "0"},};

                break;

            }


        }
    }


}
