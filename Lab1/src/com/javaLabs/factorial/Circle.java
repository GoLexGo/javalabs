package com.javaLabs.factorial;

import java.util.Scanner;

public class Circle extends Figure implements IPrintable, IInputChecker
{
    private float radius;

    @Override
    public String GetSignature()
    {
        return "Circle";
    }

    public Circle()
    {
        center = new Vector2();
        radius = 1f;
    }

    public Circle(Vector2 center, float radius)
    {
        this.center = center;
        this.radius = radius <= 0 ? 1 : radius;
    }

    public Circle(Scanner scanner)
    {
        radius = InputCheckFloat(scanner, "Radius: ");

        super.InitCenter(scanner);
    }

    public double GetSquare()
    {
        return Math.PI * Math.pow(radius, 2);
    }

    @Override
    public String toString()
    {
        return super.toString() + "Radius: "+radius;
    }

    public boolean equels(Circle circle)
    {
        return circle.radius == this.radius;
    }

}