package com.javaLabs.factorial;

import java.util.Scanner;

public abstract class Figure implements ICallable, IPrintable
{
    protected Vector2 center;

    public String GetSignature(){ return "Figure";}

    public Figure()
    {

    }

    public void InitCenter(Scanner scanner)
    {
        center = new Vector2(scanner);
    }

    public abstract double GetSquare();

    public String toString()
    {
        return GetSignature() +  " --> Center: "+ this.center.toString();
    }

    public void OutputSignature()
    {
        Print(GetSignature());
    }

}
