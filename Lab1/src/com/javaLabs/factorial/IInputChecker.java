package com.javaLabs.factorial;

import java.util.Scanner;

public interface IInputChecker extends IPrintable
{
    public default float InputCheckFloat(Scanner scanner, String message)
    {
        String state = "begin";
        float value = 0;
        do
        {
            state = "begin";
            try
            {
                Print(message);
                String inputValue = scanner.nextLine();
                value = Float.parseFloat(inputValue);
                if (value == Float.POSITIVE_INFINITY || value == Float.NEGATIVE_INFINITY)
                {
                    throw new Exception("Infinity value detected");
                }
                if (value <= 0)
                {
                    throw new Exception("Value is bellow zero or equal zero");
                }
            }
            catch (Exception e)
            {
                Print(e.getMessage());
                state = e.getMessage();
            }
            finally
            {
                Print("finally: " );
            }

        }while (!state.equals("begin"));
        return value;
    }

    public default int InputCheckInt(Scanner scanner, String message, boolean checkNull)
    {
        String state = "begin";
        int value = 0;
        do
        {
            state = "begin";
            try
            {
                Print(message);
                String inputValue = scanner.nextLine();
                value = Integer.parseInt(inputValue);
                if (value == Float.POSITIVE_INFINITY || value == Float.NEGATIVE_INFINITY)
                {
                    throw new Exception("Infinity value detected");
                }

                if(checkNull)
                {
                    if(value <= 0)
                    {
                        throw new Exception("Value must be higher than 0");
                    }
                }

            }
            catch (Exception e)
            {
                Print(e.getMessage());
                state = e.getMessage();
            }
            finally
            {
                Print("finally: " );
            }
        }while (!state.equals("begin"));
        return value;
    }
}
