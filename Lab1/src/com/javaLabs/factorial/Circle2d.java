package com.javaLabs.factorial;

import java.util.Scanner;

import static java.lang.Math.pow;
import static java.lang.StrictMath.sqrt;

public class Circle2d implements Shape, IInputChecker, IPrintable
{
    private Vector2 center;
    private double radius;

    public Circle2d()
    {
        center = IVector2.zero;
        radius = 1;
    }

    public Circle2d(Scanner scanner)
    {
        radius = InputCheckFloat(scanner, "Radius: ");
        center = new Vector2(scanner);
    }

    @Override
    public double S()
    {
        return Math.PI * pow(radius, 2);
    }

    public double Lenght()
    {
        return Math.PI * D();
    }

    @Override
    public void OutputShape()
    {
        Print("Circle");
        Print("Center:" + center.toString());
        Print("Radius: " + radius);
        Print("Square: " + S());
        Print("Lengh: " + Lenght());
    }

    public void Move(Vector2 direction)
    {
        center.Modify(direction);
    }

    public void SetCenter(Vector2 center)
    {
        this.center = center;
    }

    public void MoveRandom(int min, int max)
    {
        center.ModifyRandom(min, max);
    }

    public void Resize(float newRadius)
    {
        //todo: abs use
        if (newRadius <= 0)
        {
            Print("Radius must be higher than null(0)");
            return;
        }
        radius = newRadius;
    }

    public double GetDistance(Circle2d circle2d)
    {
        return center.GetDistance(circle2d.center);
    }

    public boolean EquelsSquare(Circle2d circle)
    {
        return circle.S() == this.S();
    }

    public void ShowTouch(Circle2d circle2d)
    {
        double x0,y0;//координаты точки пересечения всех линий

        double a;//расстояние от r1 до точки пересечения всех линий
        double h;//расстояние от точки пересеч окружностей до точки пересеч всех линий
        double d = GetDistance(circle2d);//расстояние между центрами окружностей
        Vector2 oneTouchCoord = new Vector2();
        Vector2 secondTouchCoord = new Vector2();

        if(d > this.radius+circle2d.radius)
        {
            Print("Oкружности не пересекаются");
            return;
        }
        double r1 = this.radius;
        double r2 = circle2d.radius;
        double x1 = this.center.x;
        double y1 = this.center.y;
        double x2 = circle2d.center.x;
        double y2 = circle2d.center.y;
        a = (r1*r1 - r2*r2 + d*d ) / (2*d);
        h = sqrt( pow(r1,2) - pow(a,2));

        x0 = x1 + a*( x2 - x1 ) / d;
        y0 = y1 + a*( y2 - y1 ) / d;

        double xTouch =  x0 + h*( y2 - y1 ) / d;
        double yTouch =  y0 - h*( x2 - x1 ) / d;
        oneTouchCoord = new Vector2((int)xTouch, (int)yTouch);

        if(a == r1 )
        {
            Print("Oкружности соприкасаются в точке: " + oneTouchCoord.toString());
            return;
        }

        xTouch= x0 - h*( y2 - y1 ) / d;
        yTouch= y0 + h*( x2 - x1 ) / d;
        secondTouchCoord = new Vector2((int)xTouch, (int)yTouch);

        Print("Oкружности пересекаются в точках: " + oneTouchCoord.toString() + " и "+ secondTouchCoord.toString());
    }

    private double D() // diametr
    {
        return 2*radius;
    }

}
