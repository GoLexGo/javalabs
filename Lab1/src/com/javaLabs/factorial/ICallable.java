package com.javaLabs.factorial;

public interface ICallable
{
    public String GetSignature();
}
