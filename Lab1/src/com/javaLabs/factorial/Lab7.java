package com.javaLabs.factorial;


public class Lab7
{
    public static void main(String[] args)
    {
        /*CircleContainer circleContainer = new CircleContainer(
                        new Circle[]
                        {
                            new Circle(Vector2.zero,12f),
                            new Circle(new Vector2(3,5),11f)
                        });
        circleContainer.FindEqualSquaers();*/

       FigureContainer circleContainer1 = new FigureContainer(
                new Circle[]
                        {
                                new Circle(IVector2.zero,11f),
                                new Circle(new Vector2(3,5),11f)
                        });
        circleContainer1.FindEqualSquaers();
        circleContainer1.ToString();

        FigureContainer kvadrContainer = new FigureContainer(
                new Kvadr[]
                        {
                                new Kvadr(IVector2.zero, 5f),
                                new Kvadr(IVector2.zero, 2f),
                                new Kvadr(IVector2.zero, 5f)
                        });
        kvadrContainer.FindEqualSquaers();
        kvadrContainer.ToString();
        Kvadr kvadr1 = new Kvadr(  6f);
        Kvadr kvadr2 = new Kvadr(6f);
        if(kvadr1.equels(kvadr2))
        {
            OutputUtils.Print("Kvadr1 & Kvadr2 are equal");
        }
        else
        {
            OutputUtils.Print("Kvadr1 & Kvadr2 are not equal");
        }
    }

}


