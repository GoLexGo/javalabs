package com.javaLabs.factorial;

public interface Shape
{
    //отримання площини
    public double S();

    // вивід кола на екран
    public void OutputShape();
}

